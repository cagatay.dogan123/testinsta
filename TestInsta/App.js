import React from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import MainScreen from './screens/MainScreen'

export default class App extends React.Component {
  render() {
    return (
      <AppContainer />
    );
  }
}

const StackNavigator = createStackNavigator({
  Main: {
    screen: MainScreen
  }
});

const AppContainer = createAppContainer(StackNavigator);
