import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity } from 'react-native';

class StoryScreen extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      image: require('../assets/plant.jpeg')
    }
  }

  async componentDidMount() {
    setTimeout(() => {
      this.setState({ image: require('../assets/sparkles.jpg') })
    }, 3000);
    setTimeout(() => {
      this.props.navigation.navigate('Home');
    }, 6000);
  }

  next() {
    if (this.state.image === require('../assets/plant.jpeg')) {
      this.setState({ image: require('../assets/sparkles.jpg') })
    }
    else if (this.state.image === require('../assets/sparkles.jpg')) {
      this.props.navigation.navigate('Home');
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.container}
          onPress={() => this.next()}>
          <Image
            style={styles.backgroundImage}
            source={this.state.image}
          />
        </TouchableOpacity>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
  },
});

export default StoryScreen;