import React from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';

class HomeScreen extends React.Component {

  render() {
    return (
      <ScrollView style={styles.scrollContainer}>
        <View title='stories' style={{ height: 130, borderBottomWidth: 1, borderBottomColor: '#d1cece', shadowOffset: { width: 10, height: 10, }, shadowColor: 'black', shadowOpacity: 1.0, shadowRadius: 2, elevation: 1 }}>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 7 }}>
            <Text style={{ fontWeight: 'bold' }}>Stories</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Icon name='md-play' style={{ fontSize: 14 }} />
              <Text style={{ fontWeight: 'bold' }}> Watch all</Text>
            </View>
          </View>
          <View style={{ flex: 3 }}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{ alignItems: 'center', paddingStart: 5, paddingEnd: 5 }}
            >
              <View style={{ marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                <TouchableOpacity
                  //style={{ width: 60, height: 60, borderRadius: 60 / 2, marginHorizontal: 5, borderColor: 'pink', borderWidth: 2 }}
                  onPress={() => this.props.navigation.navigate('Story')}>
                  <Image
                    source={require('../assets/harry.jpg')}
                    style={{ width: 60, height: 60, borderRadius: 60 / 2, borderColor: 'pink', borderWidth: 3 }}
                  />
                </TouchableOpacity>
                <Text>Your Story</Text>
              </View>

              <View style={{ marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Image
                  source={require('../assets/voldemort.jpg')}
                  style={{ width: 60, height: 60, borderRadius: 60 / 2, marginHorizontal: 5, borderColor: '#d1cece', borderWidth: 1 }}
                />
                <Text>voldy</Text>
              </View>
              <View style={{ marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Image
                  source={require('../assets/hermione.jpg')}
                  style={{ width: 60, height: 60, borderRadius: 60 / 2, marginHorizontal: 5, borderColor: '#d1cece', borderWidth: 1 }}
                />
                <Text>hermin</Text>
              </View>
              <View style={{ marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Image
                  source={require('../assets/snape.jpg')}
                  style={{ width: 60, height: 60, borderRadius: 60 / 2, marginHorizontal: 5, borderColor: '#d1cece', borderWidth: 1 }}
                />
                <Text>always</Text>
              </View>
              <View style={{ marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Image
                  source={require('../assets/luna.jpg')}
                  style={{ width: 60, height: 60, borderRadius: 60 / 2, marginHorizontal: 5, borderColor: '#d1cece', borderWidth: 1 }}
                />
                <Text>pudding</Text>
              </View>
              <View style={{ marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Image
                  source={require('../assets/neville.jpeg')}
                  style={{ width: 60, height: 60, borderRadius: 60 / 2, marginHorizontal: 5, borderColor: '#d1cece', borderWidth: 1 }}
                />
                <Text>nevale</Text>
              </View>
              <View style={{ marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Image
                  source={require('../assets/bellatrix.jpg')}
                  style={{ width: 60, height: 60, borderRadius: 60 / 2, marginHorizontal: 5, borderColor: '#d1cece', borderWidth: 1 }}
                />
                <Text>bellatrix</Text>
              </View>
              <View style={{ marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Image
                  source={require('../assets/ron.jpg')}
                  style={{ width: 60, height: 60, borderRadius: 60 / 2, marginHorizontal: 5, borderColor: '#d1cece', borderWidth: 1 }}
                />
                <Text>ron</Text>
              </View>
              <View style={{ marginBottom: 10, justifyContent: 'center', alignItems: 'center' }}>
                <Image
                  source={require('../assets/malfoy.jpg')}
                  style={{ width: 60, height: 60, borderRadius: 60 / 2, marginHorizontal: 5, borderColor: '#d1cece', borderWidth: 1 }}
                />
                <Text>malfoy</Text>
              </View>
            </ScrollView>
          </View>
        </View>

        <View title='post'>
          <View style={styles.topPost}>
            <View style={{ alignSelf: 'flex-start', flexDirection: 'row' }}>
              <Image
                source={require('../assets/harry.jpg')}
                style={{ width: 50, height: 50, borderRadius: 50 / 2, marginLeft: 10, marginTop: 10 }}
              />
              <View style={{ marginLeft: 4, marginTop: 14 }}>
                <Text style={{ fontWeight: "bold" }}>harryPotter</Text>
                <Text>Hogwarts, England</Text>
              </View>
            </View>
            <Icon name='ios-more' style={{ alignSelf: 'flex-end', marginRight: 10 }} />
          </View>
          <Image
            source={require('../assets/hogwarts.png')}
            style={{ flex: 1, width: null, height: 200, marginTop: 10 }}
          />
          <View style={styles.bottomPost}>
            <View style={{ alignSelf: 'flex-start', flexDirection: 'row' }}>
              <Image
                source={require('../assets/like.png')}
                style={{ width: 24 * 1.3, height: 22 * 1.3 }}
              />
              <Image
                source={require('../assets/comment.png')}
                style={{ width: 23 * 1.3, height: 23 * 1.3, marginHorizontal: 10 }}
              />
              <Image
                source={require('../assets/sendit.png')}
                style={{ width: 74 / 2.3, height: 66 / 2.3, marginTop: 1 }}
              />
            </View>
            <Image
              source={require('../assets/save.png')}
              style={{ width: 17 * 1.3, height: 21 * 1.3, alignSelf: 'flex-end' }}
            />
          </View>
          <Text style={{ marginLeft: 10 }}>Liked by <Text style={{ fontWeight: 'bold' }}>hermoniefanclubb</Text> and <Text style={{ fontWeight: 'bold' }}>621 others</Text></Text>
          <Text style={{ marginLeft: 10, marginTop: 10 }}>
            <Text style={{ fontWeight: 'bold' }}>harryPotter </Text>
            <Text>Hogwarts'ta sıradan bir gün </Text>
            <Text style={{ color: 'blue' }}>#tbt #dumbledore'un_cocuklarıyız</Text>
          </Text>
        </View>

      </ScrollView>
    );
  }
}

export default HomeScreen;

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: '#fff'
  },
  topPost: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  bottomPost: {
    margin: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
  }
});