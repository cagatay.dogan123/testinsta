import React from 'react';
import { StyleSheet, Platform, Image } from 'react-native';
import { Icon } from 'native-base';
import { createBottomTabNavigator, createAppContainer, createStackNavigator } from 'react-navigation';

import HomeScreen from './HomeScreen';
import SearchScreen from './SearchScreen';
import AddMediaScreen from './AddMediaScreen';
import LikesScreen from './LikesScreen';
import ProfileScreen from './ProfileScreen';
import StoryScreen from './StoryScreen';

class MainScreen extends React.Component {

  static navigationOptions = {
    headerLeft:
      //<Icon name='md-camera' style={{ paddingLeft: 10 }} />,
      <Image
        source={require('../assets/camera.png')}
        style={{ width: 45, height: 45, marginLeft: 10 }}
      />,
    title: 'Instagram',
    headerTitleStyle: {
      flex: 1,
      textAlign: "center"
    },
    headerRight:
      //<Icon name='ios-send' style={{ paddingRight: 10 }} />
      <Image
        source={require('../assets/sendit.png')}
        style={{ width: 74 / 2.3, height: 66 / 2.3, marginRight: 10 }}
      />
  }

  render() {
    return (
      <AppContainer />
    );
  }
}

const HomeStoryNavigator = createStackNavigator({
  Home: {
    screen: HomeScreen
  },
  Story: {
    screen: StoryScreen
  }
}, {
    headerMode: 'none',
    navigationOptions: {
      tabBarIcon: ({ tintColor }) => (
        <Icon name='home' style={{ color: tintColor }} />
      )
    }
  }
);

const TabNavigator = createBottomTabNavigator({
  Home: HomeStoryNavigator,
  Search: {
    screen: SearchScreen
  },
  AddMedia: {
    screen: AddMediaScreen
  },
  Likes: {
    screen: LikesScreen
  },
  Profile: {
    screen: ProfileScreen
  }
}, {
    animationEnabled: true,
    swipeEnabled: true,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      style: {
        ...Platform.select({
          android: {
            backgroundColor: 'white'
          }
        })
      },
      activeTintColor: '#000',
      inactiveTintColor: '#d1cece',
      showLabel: false,
      showIcon: true
    }
  });

const AppContainer = createAppContainer(TabNavigator);

export default MainScreen;